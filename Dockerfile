FROM golang:1.10

EXPOSE 8080

WORKDIR /go/src/gitlab.com/shereland/graphql
COPY . .
ENV GOBIN=/go/bin

RUN go get -d -v ./...
RUN go install -v ./...
RUN go get github.com/githubnemo/CompileDaemon

VOLUME [ "/var/log/graphql" ]

CMD CompileDaemon -build='go install main/main.go main/mail.go main/relay.go main/server.go' -command='main'
