
build-development:
	docker build -t registry.gitlab.com/shereland/graphql:development .

build-production:
	docker build -t registry.gitlab.com/shereland/graphql:stable -f production.Dockerfile .
