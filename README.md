# Shereland GraphQL

Graphql is the microservice only for receiving the front requests from web.

*\* Today, it is starting as monolithic service, receiving requests, and treating them, just for a simple start. At the future, it will be only the interface with other microservices.*

*Generic* is supposed to be a temporary solution for retrieving data. It is being used to release early the new version of Shereland. It is expected to be replaced by new microservices, and graphql is only a server for accessing them.
