package database

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql" // required for mysql connection
)

var (
	// DBCon is the connection handle
	// for the database
	DBCon *sql.DB
)
