package database

import (
	"github.com/go-redis/redis"
)

var (
	// DBRedisCon is the connection handle
	// for the database
	DBRedisCon *redis.Client
)
