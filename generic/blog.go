package generic

import (
	"database/sql"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"math"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/nfnt/resize"
	"gitlab.com/shereland/graphql/database"
	"gitlab.com/shereland/graphql/logging"
)

type post struct {
	id             string
	views          string
	title          string
	slug           string
	category       string
	categorySlug   string
	content        string
	publishedAt    time.Time
	authorUsername string
	comments       []map[string]string
}

type comment struct {
	id        string
	name      string
	site      string
	content   string
	postID    string
	createdAt time.Time
}

type relatedBook struct {
	postID   string
	title    string
	bookSlug string
}

type relatedPost struct {
	postID           string
	title            string
	postSlug         string
	postCategorySlug string
}

type album struct {
	postID      string
	title       string
	photosURL   []string
	photosTitle []string
	photosAlt   []string
}

type photo struct {
	postID       string
	albumTitle   string
	url          string
	thumbnailURL string
	title        string
	alt          string
}

// GetPopularPosts get most popular posts from last day
func GetPopularPosts() []map[string]string {
	methodLog := logging.Log.WithFields(log.Fields{
		"method": "GetPopularPosts",
	})

	postsArray := make([]map[string]string, 0, 5)
	client := database.DBRedisCon
	yesterday := time.Now().Local().AddDate(0, 0, -1)
	keyRedis := "post_views:" + yesterday.Format("20060102")
	posts := client.ZRevRangeWithScores(keyRedis, 0, 5)
	slugs := make([]string, 0, 5)
	for key := range posts.Val() {
		member := posts.Val()[key].Member
		slugs = append(slugs, "\""+member.(string)+"\"")
	}

	if len(slugs) == 0 {
		return postsArray
	}

	inClause := "(" + strings.Join(slugs, ", ") + ")"

	db := database.DBCon
	query := `
		SELECT blog_post.title AS postTitle, blog_post.slug AS postSlug,
			blog_category.slug AS categorySlug
		FROM blog_post
		INNER JOIN blog_category
			ON blog_post.category_id = blog_category.id
		WHERE blog_post.slug IN ` + inClause + `
		LIMIT 5
		;
	`

	rows, err := db.Query(query)
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("db.Query(query)")
	}
	defer rows.Close()

	for rows.Next() {
		var p post
		if err := rows.Scan(&p.title, &p.slug, &p.categorySlug); err != nil {
			methodLog.WithFields(log.Fields{
				"err": err,
			}).Error("rows.Scan(&p.title, &p.slug, &p.categorySlug)")
		}
		post := make(map[string]string)
		post["title"] = p.title
		post["url"] = "/blog/livros/" + p.categorySlug + "/" + p.slug
		postsArray = append(postsArray, post)
	}
	if rows.Err() != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("rows.Err()")
	}

	return postsArray
}

// GetPost return only one post
func GetPost(slug string) map[string]interface{} {
	methodLog := logging.Log.WithFields(log.Fields{
		"method": "GetPost",
	})

	db := database.DBCon
	singlepost := make(map[string]interface{})

	query := `SELECT blog_post.id, blog_post.slug, blog_post.title, blog_post.content,
					 blog_post.published_at, blog_category.slug, blog_category.title,
					 auth_user.username
	FROM blog_post
	INNER JOIN blog_category
		ON blog_category.id = blog_post.category_id
	INNER JOIN auth_user
		ON auth_user.id = blog_post.author_id
	WHERE blog_post.slug = ?
	ORDER BY blog_post.published_at DESC
	LIMIT 1`

	var rows *sql.Rows
	rows, err := db.Query(query, slug)
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("db.Query(query, slug)")
	}
	defer rows.Close()

	postsIDArray := make([]string, 0, 1)
	for rows.Next() {
		var p post
		if err := rows.Scan(&p.id, &p.slug, &p.title, &p.content, &p.publishedAt, &p.categorySlug,
			&p.category, &p.authorUsername); err != nil {

			methodLog.WithFields(log.Fields{
				"err": err,
			}).Error("rows.Scan(&p.id, &p.slug, &p.title, &p.content...")
		}
		post := make(map[string]interface{})
		post["id"] = p.id
		post["slug"] = p.slug
		post["title"] = p.title
		post["content"] = p.content
		post["categorySlug"] = p.categorySlug
		post["category"] = p.category
		post["publishedAt"] = strconv.FormatInt(p.publishedAt.Unix(), 10)
		post["url"] = "/blog/livros/" + p.categorySlug + "/" + p.slug

		author := make(map[string]interface{})
		author["username"] = p.authorUsername
		post["author"] = author

		postsIDArray = append(postsIDArray, p.id)
		singlepost = post
	}
	if rows.Err() != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("rows.Err()")
	}
	if len(postsIDArray) == 0 {
		return nil
	}

	// One-to-many queries
	comments := getComments(db, postsIDArray)
	relatedBooks := getRelatedBooks(db, postsIDArray)
	relatedPosts := getRelatedPosts(db, postsIDArray)
	albuns := getAlbuns(db, postsIDArray)

	_, commentsExist := comments[singlepost["id"].(string)]
	if commentsExist {
		singlepost["comments"] = comments[singlepost["id"].(string)]
	}

	_, relatedBooksExist := relatedBooks[singlepost["id"].(string)]
	if relatedBooksExist {
		singlepost["relatedBooks"] = relatedBooks[singlepost["id"].(string)]
	}

	_, relatedPostsExist := relatedPosts[singlepost["id"].(string)]
	if relatedPostsExist {
		singlepost["relatedPosts"] = relatedPosts[singlepost["id"].(string)]
	}

	_, albumExist := albuns[singlepost["id"].(string)]
	if albumExist {
		singlepost["album"] = albuns[singlepost["id"].(string)]
	}

	go incrementPostVisit(slug)

	return singlepost
}

func incrementPostVisit(slug string) {
	client := database.DBRedisCon

	currentTime := time.Now().Local()
	key := "post_views:" + currentTime.Format("20060102")
	client.ZIncrBy(key, 1, slug)
}

// GetPosts return the posts asked
func GetPosts(category string, page int32) []map[string]interface{} {
	methodLog := logging.Log.WithFields(log.Fields{
		"category": category,
		"page":     page,
		"method":   "GetPosts",
	})

	db := database.DBCon
	postsArray := make([]map[string]interface{}, 0, 4)

	var whereClause string
	if category != "" {
		whereClause = "WHERE blog_category.slug = ? AND blog_post.published_at IS NOT NULL"
	} else {
		whereClause = "WHERE blog_post.published_at IS NOT NULL"
	}

	query := `SELECT blog_post.id, blog_post.slug, blog_post.title, blog_post.content,
					 blog_post.published_at, blog_category.slug,
					 auth_user.username
	FROM blog_post
	INNER JOIN blog_category
		ON blog_category.id = blog_post.category_id
	INNER JOIN auth_user
		ON auth_user.id = blog_post.author_id
	` + whereClause + `
	ORDER BY blog_post.published_at DESC
	LIMIT 4 OFFSET ?
	`

	var rows *sql.Rows
	var err error
	offset := (page - 1) * 4
	if category != "" {
		rows, err = db.Query(query, category, offset)
	} else {
		rows, err = db.Query(query, offset)
	}
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("db.Query(query[, category], offset)")

		return make([]map[string]interface{}, 0, 0)
	}
	defer rows.Close()

	postsIDArray := make([]string, 0, 4)
	for rows.Next() {
		var p post
		if err := rows.Scan(&p.id, &p.slug, &p.title, &p.content, &p.publishedAt, &p.categorySlug,
			&p.authorUsername); err != nil {

			methodLog.WithFields(log.Fields{
				"err": err,
			}).Error("rows.Scan(&p.id, &p.slug, &p.title, &p.content,...")
		}
		post := make(map[string]interface{})
		post["id"] = p.id
		post["slug"] = p.slug
		post["title"] = p.title
		post["content"] = p.content
		post["publishedAt"] = strconv.FormatInt(p.publishedAt.Unix(), 10)
		post["url"] = "/blog/livros/" + p.categorySlug + "/" + p.slug

		author := make(map[string]interface{})
		author["username"] = p.authorUsername
		post["author"] = author

		postsIDArray = append(postsIDArray, p.id)
		postsArray = append(postsArray, post)
	}
	if rows.Err() != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("rows.Err()")
	}

	// If no posts are found, it should not make further queries
	if len(postsIDArray) == 0 {
		return make([]map[string]interface{}, 0, 0)
	}

	// One-to-many queries
	comments := getComments(db, postsIDArray)
	relatedBooks := getRelatedBooks(db, postsIDArray)
	relatedPosts := getRelatedPosts(db, postsIDArray)
	albuns := getAlbuns(db, postsIDArray)

	for _, post := range postsArray {
		_, commentsExist := comments[post["id"].(string)]
		if commentsExist {
			post["comments"] = comments[post["id"].(string)]
		}

		_, relatedBooksExist := relatedBooks[post["id"].(string)]
		if relatedBooksExist {
			post["relatedBooks"] = relatedBooks[post["id"].(string)]
		}

		_, relatedPostsExist := relatedPosts[post["id"].(string)]
		if relatedPostsExist {
			post["relatedPosts"] = relatedPosts[post["id"].(string)]
		}

		_, albumExist := albuns[post["id"].(string)]
		if albumExist {
			post["album"] = albuns[post["id"].(string)]
		}
	}

	return postsArray
}

func getComments(db *sql.DB, postsIDArray []string) map[string][]map[string]string {
	methodLog := logging.Log.WithFields(log.Fields{
		"postsIDArray": postsIDArray,
		"method":       "getComments",
	})

	comments := make(map[string][]map[string]string, 0)
	queryIDs := strings.Join(postsIDArray, ", ")
	query := `SELECT blog_comment.id, blog_comment.name, blog_comment.content, blog_comment.site,
					blog_comment.post_id, blog_comment.created_at
	FROM blog_comment
	WHERE blog_comment.post_id IN (` + queryIDs + `)`
	rows, _ := db.Query(query)
	for rows.Next() {
		var c comment
		if err := rows.Scan(&c.id, &c.name, &c.content, &c.site, &c.postID, &c.createdAt); err != nil {
			methodLog.WithFields(log.Fields{
				"err": err,
			}).Error("rows.Scan(&c.id, &c.name, &c.content, &c.site, &c.postID, &c.createdAt)")
		}

		_, keyExist := comments[c.postID]
		if !keyExist {
			comments[c.postID] = make([]map[string]string, 0, 2)
		}
		comment := make(map[string]string)
		comment["name"] = c.name
		comment["content"] = c.content
		comment["site"] = c.site
		comment["createdAt"] = strconv.FormatInt(c.createdAt.Unix(), 10)
		comments[c.postID] = append(comments[c.postID], comment)
	}

	return comments
}

func getRelatedBooks(db *sql.DB, postsIDArray []string) map[string][]map[string]string {
	methodLog := logging.Log.WithFields(log.Fields{
		"postsIDArray": postsIDArray,
		"method":       "getRelatedBooks",
	})

	relatedBooks := make(map[string][]map[string]string, 0)
	queryIDs := strings.Join(postsIDArray, ", ")
	query := `SELECT book_book.title, book_book.slug, book_book_related_posts.post_id
	FROM book_book_related_posts
	INNER JOIN book_book
		ON book_book_related_posts.post_id IN (` + queryIDs + `) AND
		   book_book_related_posts.book_id = book_book.id`
	rows, _ := db.Query(query)
	for rows.Next() {
		var r relatedBook
		if err := rows.Scan(&r.title, &r.bookSlug, &r.postID); err != nil {
			methodLog.WithFields(log.Fields{
				"err": err,
			}).Error("rows.Scan(&r.title, &r.bookSlug, &r.postID)")
		}

		_, keyExist := relatedBooks[r.postID]
		if !keyExist {
			relatedBooks[r.postID] = make([]map[string]string, 0, 2)
		}
		relatedBook := make(map[string]string)
		relatedBook["title"] = r.title
		relatedBook["link"] = "/livros/" + r.bookSlug
		relatedBooks[r.postID] = append(relatedBooks[r.postID], relatedBook)
	}

	return relatedBooks
}

func getRelatedPosts(db *sql.DB, postsIDArray []string) map[string][]map[string]string {
	methodLog := logging.Log.WithFields(log.Fields{
		"postsIDArray": postsIDArray,
		"method":       "getRelatedPosts",
	})

	relatedPosts := make(map[string][]map[string]string, 0)
	queryIDs := strings.Join(postsIDArray, ", ")
	query := `SELECT blog_post.title, blog_post.slug, blog_category.slug,
				   blog_post_related_posts.to_post_id
	FROM blog_post_related_posts
	INNER JOIN blog_post
		ON blog_post_related_posts.from_post_id = blog_post.id
	LEFT JOIN blog_category
		ON blog_category.id = blog_post.category_id
	WHERE blog_post_related_posts.to_post_id IN (` + queryIDs + `)`
	rows, _ := db.Query(query)
	for rows.Next() {
		var r relatedPost
		if err := rows.Scan(&r.title, &r.postSlug, &r.postCategorySlug, &r.postID); err != nil {
			methodLog.WithFields(log.Fields{
				"err": err,
			}).Error("rows.Scan(&r.title, &r.postSlug, &r.postCategorySlug, &r.postID)")
		}

		_, keyExist := relatedPosts[r.postID]
		if !keyExist {
			relatedPosts[r.postID] = make([]map[string]string, 0, 2)
		}
		relatedPost := make(map[string]string)
		relatedPost["title"] = r.title
		relatedPost["link"] = "/blog/livros/" + r.postCategorySlug + "/" + r.postSlug
		relatedPosts[r.postID] = append(relatedPosts[r.postID], relatedPost)
	}

	return relatedPosts
}

func getAlbuns(db *sql.DB, postsIDArray []string) map[string]interface{} {
	methodLog := logging.Log.WithFields(log.Fields{
		"postsIDArray": postsIDArray,
		"method":       "getAlbuns",
	})

	// albuns := make(map[string][]map[string]string, 0)
	albuns := make(map[string]interface{}, 0)
	queryIDs := strings.Join(postsIDArray, ", ")
	query := `SELECT blog_post.id, blog_album.title, blog_photo.image, blog_photo.image_thumbnail,
					 blog_photo.title, blog_photo.alt
	FROM blog_post
	INNER JOIN blog_album
		ON blog_post.album_id = blog_album.id
	INNER JOIN blog_photo
		ON blog_album.id = blog_photo.author_id
	WHERE blog_post.id IN (` + queryIDs + `)`
	rows, _ := db.Query(query)
	for rows.Next() {
		var p photo
		if err := rows.Scan(&p.postID, &p.albumTitle, &p.url, &p.thumbnailURL, &p.title, &p.alt); err != nil {
			methodLog.WithFields(log.Fields{
				"err": err,
			}).Error("rows.Scan(&p.postID, &p.albumTitle, &p.url, &p.thumbnailURL, &p.title, &p.alt)")
		}

		_, keyExist := albuns[p.postID]
		if !keyExist {
			albuns[p.postID] = make(map[string]interface{})
			albuns[p.postID].(map[string]interface{})["title"] = p.albumTitle
			albuns[p.postID].(map[string]interface{})["photos"] = make([]map[string]string, 0, 10)
		}
		photo := make(map[string]string)
		mediaURL := os.Getenv("MEDIA_URL")
		photo["title"] = p.title
		photo["alt"] = p.alt
		photo["image"] = mediaURL + "/" + p.url
		photo["thumbnail"] = getThumbnail(db, mediaURL, p.url, p.thumbnailURL)

		album := albuns[p.postID].(map[string]interface{})["photos"]
		album = append(album.([]map[string]string), photo)
		albuns[p.postID].(map[string]interface{})["photos"] = album
	}

	return albuns
}

// getThumbnail if thumbnail exist, just return it
// Otherwise:
// - Download the original
// - Resize
// - Save on S3 and on database
// - Return the thumbnail
// This is a slow function, but should be fast on most cases
// (thumbnail is persisted on db, so it should be called only on first time
// the album is opened)
func getThumbnail(db *sql.DB, mediaURL, photoURL, thumbnailURL string) string {
	methodLog := logging.Log.WithFields(log.Fields{
		"mediaURL":     mediaURL,
		"photoURL":     photoURL,
		"thumbnailURL": thumbnailURL,
		"method":       "FriendshipNotifications",
	})

	if thumbnailURL != "" {
		return mediaURL + "/" + thumbnailURL
	}

	if os.Getenv("ENVIRONMENT") != "production" {
		return mediaURL + "/" + thumbnailURL
	}

	var err error
	fileExtension := filepath.Ext(photoURL)
	rand.Seed(time.Now().Unix())
	randomImageName := "img-" + strconv.Itoa(rand.Int())
	tempFileLocation := "/tmp/" + randomImageName + fileExtension
	s3File := mediaURL + "/" + photoURL
	fileNoExtension := strings.TrimSuffix(photoURL, filepath.Ext(photoURL))

	// Download the original
	response, err := http.Get(s3File)
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("http.Get(s3File)")
	}
	defer response.Body.Close()

	// Resize
	var img image.Image
	if fileExtension == ".jpg" || fileExtension == ".jpeg" {
		img, err = jpeg.Decode(response.Body)
	} else if fileExtension == ".gif" {
		img, err = gif.Decode(response.Body)
	} else if fileExtension == ".png" {
		img, err = png.Decode(response.Body)
	} else {
		methodLog.Warn("getThumbnail error: Format not recognized")
	}
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error(".Decode(response.Body)")
	}

	m := resize.Resize(0, 79, img, resize.Lanczos3)

	// Save local
	out, err := os.Create(tempFileLocation)
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error(".Decode(response.Body)")
	}
	defer out.Close()
	png.Encode(out, m)

	// Upload to S3
	bucket := os.Getenv("AWS_S3_BUCKET")
	acl := "public-read"
	file, err := os.Open(tempFileLocation)
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err":              err,
			"tempFileLocation": tempFileLocation,
		}).Error("os.Open(tempFileLocation)")
	}
	defer file.Close()

	conf := aws.Config{Region: aws.String(os.Getenv("AWS_S3_REGION"))}
	sess := session.New(&conf)
	svc := s3manager.NewUploader(sess)
	contentType := "image/png"

	// Save thumbnails always as png (gif doesn't have good resolution)
	// But keep extension to avoid overwrites of the same filename
	// Eg.: image.gif and image.jpg would both be saved as image-thumbnail.png
	imageThumbnailName := fileNoExtension + fileExtension + "-thumbnail.png"
	_, err = svc.Upload(&s3manager.UploadInput{
		Bucket:      aws.String(bucket),
		Key:         aws.String("media/" + imageThumbnailName),
		Body:        file,
		ACL:         &acl,
		ContentType: &contentType,
	})
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("svc.Upload(&s3manager.UploadInput")
	}

	// Update database
	query := `UPDATE blog_photo
		SET image_thumbnail=?
		WHERE image=?`
	stmt, err := db.Prepare(query)
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("db.Prepare(query)")
	}
	defer stmt.Close()
	stmt.Exec(imageThumbnailName, photoURL)

	return mediaURL + "/" + imageThumbnailName
}

// GetNumberOfPages return number of pages
func GetNumberOfPages(category string) int32 {
	methodLog := logging.Log.WithFields(log.Fields{
		"category":         category,
		"GetNumberOfPages": "GetNumberOfPages",
	})

	var numberOfPages int32
	var query string
	db := database.DBCon
	var rows *sql.Rows
	var err error

	if category != "" {
		query = `SELECT COUNT(*)
		FROM blog_post
		INNER JOIN blog_category
						        ON blog_category.id = blog_post.category_id
		WHERE blog_category.slug = ? AND
			  blog_post.published_at < NOW() AND
			  blog_post.published_at IS NOT NULL`
		rows, err = db.Query(query, category)
	} else {
		query = `SELECT COUNT(*)
		FROM blog_post
		WHERE published_at < NOW() AND
		published_at IS NOT NULL`
		rows, err = db.Query(query)
	}

	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("db.Query(query[, category])")
		return 10
	}
	defer rows.Close()

	for rows.Next() {
		var numberOfPosts int32
		if err := rows.Scan(&numberOfPosts); err != nil {
			methodLog.WithFields(log.Fields{
				"err": err,
			}).Error("rows.Scan(&numberOfPosts)")
		}
		numberOfPages = int32(math.Ceil(float64(numberOfPosts) / float64(4)))
	}
	if rows.Err() != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("rows.Err()")
	}

	return numberOfPages
}
