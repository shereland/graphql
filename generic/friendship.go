package generic

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/shereland/graphql/database"
	"gitlab.com/shereland/graphql/logging"
)

type user struct {
	id        string
	slug      string
	firstName string
	lastName  string
}

// FriendshipNotifications get friendship requests
func FriendshipNotifications(userid string) []map[string]string {
	methodLog := logging.Log.WithFields(log.Fields{
		"userid": userid,
		"method": "FriendshipNotifications",
	})

	notificationsArray := make([]map[string]string, 0, 2)
	db := database.DBCon

	query := `SELECT friendship_friendshiprequest.to_user_id, auth_user.username,
					 auth_user.first_name, auth_user.last_name
		FROM friendship_friendshiprequest
		INNER JOIN auth_user
			ON auth_user.id = friendship_friendshiprequest.to_user_id
		WHERE friendship_friendshiprequest.to_user_id = ?
		LIMIT 5
		;
	`

	rows, err := db.Query(query, userid)
	if err != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("Error in FriendshipNotifications")
	}
	defer rows.Close()

	for rows.Next() {
		var u user
		if err := rows.Scan(&u.id, &u.slug, &u.firstName, &u.lastName); err != nil {
			methodLog.WithFields(log.Fields{
				"err": err,
			}).Error("Error in FriendshipNotifications")
		}
		user := make(map[string]string)
		user["slug"] = u.slug
		user["name"] = u.firstName + " " + u.lastName
		notificationsArray = append(notificationsArray, user)
	}
	if rows.Err() != nil {
		methodLog.WithFields(log.Fields{
			"err": err,
		}).Error("Error in FriendshipNotifications")
	}

	return notificationsArray
}
