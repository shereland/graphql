package generic

import (
	"net/url"
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/ChimeraCoder/anaconda"
	"github.com/go-redis/redis"
	"gitlab.com/shereland/graphql/database"
	"gitlab.com/shereland/graphql/logging"
)

// GetLatestTweets Look for tweets at Redis DB
// If it doesn't exist, look at Twitter and persist at Redis DB
func GetLatestTweets() []string {
	methodLog := logging.Log.WithFields(log.Fields{
		"method": "GetLatestTweets",
	})

	var tweets []string
	client := database.DBRedisCon
	tweets = getFromRedis(client)
	if tweets == nil {
		tweets = getFromTwitter()
		methodLog.Debug(tweets)
		setToRedis(client, tweets)
	}
	return tweets
}

func getFromRedis(client *redis.Client) []string {
	methodLog := logging.Log.WithFields(log.Fields{
		"method": "getFromRedis",
	})

	tweetsArray := make([]string, 0, 2)
	tweet1, err1 := client.Get("latest_tweet1").Result()
	tweet2, err2 := client.Get("latest_tweet2").Result()
	if err1 == redis.Nil || err2 == redis.Nil {
		// Keys does not exist
		return nil
	}
	if err1 != nil || err2 != nil {
		methodLog.WithFields(log.Fields{
			"err1": err1,
			"err2": err2,
		}).Error("getFromRedis")
		return nil
	}
	if tweet1 == "" || tweet2 == "" {
		methodLog.WithFields(log.Fields{
			"err": "tweet1 and tweet2 are empty",
		}).Warn("getFromRedis")
		return nil
	}
	tweetsArray = append(tweetsArray, tweet1)
	tweetsArray = append(tweetsArray, tweet2)
	return tweetsArray
}

func setToRedis(client *redis.Client, tweets []string) {
	client.Set("latest_tweet1", tweets[0], time.Hour)
	client.Set("latest_tweet2", tweets[1], time.Hour)
}

func getFromTwitter() []string {
	methodLog := logging.Log.WithFields(log.Fields{
		"method": "getFromTwitter",
	})

	api := anaconda.NewTwitterApiWithCredentials(os.Getenv("TWITTER_OAUTH_TOKEN"),
		os.Getenv("TWITTER_OAUTH_SECRET"),
		os.Getenv("TWITTER_CONSUMER_KEY"),
		os.Getenv("TWITTER_CONSUMER_SECRET"))
	v := url.Values{}
	v.Set("count", "2")
	v.Set("screen_name", "_shereland")
	tweets, err := api.GetUserTimeline(v)
	if err != nil {
		methodLog.Error(err)
		return nil
	}
	tweetsArray := make([]string, 0, 2)
	for _, tweet := range tweets {
		tweetsArray = append(tweetsArray, tweet.Text)
	}
	return tweetsArray
}
