package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
)

func sendErrorMailToAdmin(errorMessage string) {
	conf := aws.Config{Region: aws.String(os.Getenv("AWS_SES_REGION"))}
	awsSession := session.New(&conf)
	sesSession := ses.New(awsSession)
	sesEmailInput := &ses.SendEmailInput{
		Destination: &ses.Destination{
			ToAddresses: []*string{aws.String(os.Getenv("ADMIN_EMAIL"))},
		},
		Message: &ses.Message{
			Body: &ses.Body{
				Html: &ses.Content{
					Data: aws.String(errorMessage)},
			},
			Subject: &ses.Content{
				Data: aws.String("Error on GraphQL Server"),
			},
		},
		Source: aws.String(os.Getenv("SERVER_EMAIL")),
		ReplyToAddresses: []*string{
			aws.String(os.Getenv("SERVER_EMAIL")),
		},
	}

	_, err := sesSession.SendEmail(sesEmailInput)
	if err != nil {
		fmt.Println("Send e-mail to admin failed")
		fmt.Println(err)
	}
}
