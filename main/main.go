package main

import (
	"context"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"

	"github.com/go-redis/redis"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/shereland/graphql/database"
	"gitlab.com/shereland/graphql/logging"
	"gitlab.com/shereland/graphql/metrics"
)

func main() {
	var err error

	/*
	 * Initialize databases
	 */
	dsn := os.Getenv("MYSQL_USER") + ":" + os.Getenv("MYSQL_PASSWORD") + "@(monolithic_db:3306)/shereland?parseTime=true"
	database.DBCon, err = sql.Open("mysql", dsn)
	if err != nil {
		fmt.Println(err)
	}
	database.DBRedisCon = redis.NewClient(&redis.Options{
		Addr:     "redis_db:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	prometheus.MustRegister(metrics.ResolverDurations)

	/*
	 * Logging
	 */
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)
	if os.Getenv("LOG_OUTPUT") == "file" {
		file, err := os.OpenFile("/var/log/graphql/all.log", os.O_CREATE|os.O_WRONLY, 0666)
		if err == nil {
			log.SetOutput(file)
		} else {
			log.Info("Failed to log to file, using default stderr")
		}
		log.SetOutput(file)
		var hostname string
		b, err := ioutil.ReadFile("/etc/hostname")
		if err != nil {
			hostname = "no hostname"
		} else {
			hostname = string(b)
			hostname = hostname[0 : len(hostname)-2]
		}
		logging.Log = log.WithFields(log.Fields{
			"hostname": hostname,
			"service":  "graphql",
		})
	} else {
		log.SetOutput(os.Stdout)
		logging.Log = log.WithFields(log.Fields{
			"service": "graphql",
		})
	}

	/*
	 * Handle requests
	 */
	mux := http.NewServeMux()
	mux.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write(page)
	}))

	mux.Handle("/graphql", authenticationMiddleware(&Handler{Schema: schema}))
	mux.Handle("/metrics", promhttp.Handler())

	handler := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000", "https://www.shereland.com", "https://graphql.shereland.com"},
		AllowCredentials: true,
		Debug:            false,
	}).Handler(mux)
	log.Fatal(http.ListenAndServe(":8080", handler))
}

// TODO: Change authentication to JWT
func authenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		sessionID := r.Header.Get("sessionid")

		if sessionID == "" {
			next.ServeHTTP(w, r.WithContext(context.WithValue(ctx, "sessionid", "")))
		} else {
			userDataBase64, err := database.DBRedisCon.Get(sessionID).Result()
			if err != nil {
				fmt.Println("authenticationMiddleware: error on returning user data")
				fmt.Println(sessionID)
				next.ServeHTTP(w, r.WithContext(context.WithValue(ctx, "sessionid", "")))
			} else {
				userDataFullB, _ := base64.StdEncoding.DecodeString(userDataBase64)
				userDataFull := string(userDataFullB)
				if len(userDataFull) < 41 {
					fmt.Println("authenticationMiddleware: error on user data")
					fmt.Println(sessionID)
					fmt.Println(userDataFull)
				}
				userData := string(userDataFull)[41:]
				var userJSON map[string]interface{}
				if err := json.Unmarshal([]byte(userData), &userJSON); err != nil {
					fmt.Println(err)
				}
				userID := userJSON["_auth_user_id"]
				next.ServeHTTP(w, r.WithContext(context.WithValue(ctx, "sessionid", userID)))
			}
		}
	})
}
