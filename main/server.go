package main

import (
	"io/ioutil"
	"os"

	"gitlab.com/shereland/graphql/resolvers"

	"github.com/graph-gophers/graphql-go"
)

var schema *graphql.Schema

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func init() {
	var fileLocation string
	var err error
	if _, err := os.Stat("../schema.gql"); !os.IsNotExist(err) {
		fileLocation = "../schema.gql"
	} else if _, err = os.Stat("schema.gql"); !os.IsNotExist(err) {
		fileLocation = "schema.gql"
	} else {
		panic("File schema.gql not found!")
	}
	SchemaString, errorRead := ioutil.ReadFile(fileLocation)
	check(errorRead)
	schema, err = graphql.ParseSchema(string(SchemaString), &resolvers.Resolver{})
	if err != nil {
		panic(err)
	}
}

var page = []byte(`
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/graphiql/0.10.2/graphiql.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/fetch/1.1.0/fetch.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.5.4/react.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.5.4/react-dom.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/graphiql/0.10.2/graphiql.js"></script>
	</head>
	<body style="width: 100%; height: 100%; margin: 0; overflow: hidden;">
		<div id="graphiql" style="height: 100vh;">Loading...</div>
		<script>
			function graphQLFetcher(graphQLParams) {
				return fetch("/graphql", {
					method: "post",
					body: JSON.stringify(graphQLParams),
					credentials: "include",
				}).then(function (response) {
					return response.text();
				}).then(function (responseBody) {
					try {
						return JSON.parse(responseBody);
					} catch (error) {
						return responseBody;
					}
				});
			}
			ReactDOM.render(
				React.createElement(GraphiQL, {fetcher: graphQLFetcher}),
				document.getElementById("graphiql")
			);
		</script>
	</body>
</html>
`)
