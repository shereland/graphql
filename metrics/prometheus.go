package metrics

import "github.com/prometheus/client_golang/prometheus"

var (
	ResolverDurations = prometheus.NewSummaryVec(prometheus.SummaryOpts{
		Name:       "resolvers_duration_milliseconds",
		Help:       "Time (ms) for each resolver.",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
	},
		[]string{"resolvers"},
	)
)
