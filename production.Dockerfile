FROM golang:1.10

EXPOSE 8080

WORKDIR /go/src/gitlab.com/shereland/graphql/main
COPY . ..
ENV GOBIN=/go/bin

RUN go get -d -v ./...
RUN CGO_ENABLED=0 GOOS=linux go build -o graphql -a -ldflags '-extldflags "-static"' .

FROM scratch
COPY --from=0 /go/src/gitlab.com/shereland/graphql/main/graphql .
COPY --from=0 /go/src/gitlab.com/shereland/graphql/schema.gql .
COPY --from=0 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
VOLUME [ "/var/log/graphql" ]
ENTRYPOINT ["/graphql"]
