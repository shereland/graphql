package resolvers

type Album struct {
	title  string
	photos []*Photo
}

func (a Album) Title() string {
	return a.title
}

func (a Album) Photos() *[]*Photo {
	// photos := make([]*Photo, 0, 10)
	// p := Photo{}
	// photos = append(photos, &p)
	return &a.photos
}

type Photo struct {
	thumbnail   string
	image       string
	title       string
	description string
}

func (p Photo) Thumbnail() string {
	return p.thumbnail
}

func (p Photo) Image() string {
	return p.image
}

func (p Photo) Title() string {
	return p.title
}

func (p Photo) Description() *string {
	return &p.description
}
