package resolvers

type Book struct {
	title string
	link  string
}

func (b Book) Title() *string {
	return &b.title
}

func (b Book) Link() *string {
	return &b.link
}
