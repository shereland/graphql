package resolvers

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/shereland/graphql/logging"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

type Contact struct {
	error *string
}

type bodyStruct struct {
	TemplateName    string                `json:"templateName"`
	TemplateOptions templateOptionsStruct `json:"templateOptions"`
	EmailOptions    emailOptionsStruct    `json:"emailOptions"`
}

type templateOptionsStruct struct {
	Name           string `json:"name"`
	Email          string `json:"email"`
	Phone          string `json:"phone"`
	Subject        string `json:"subject"`
	How_found_site string `json:"how_found_site"`
	Message        string `json:"message"`
}

type emailOptionsStruct struct {
	To string `json:"to"`
}

type mailServerResponse struct {
	Error *string `json:"error`
}

func prepareBody(name string,
	email string,
	phone string,
	subject string,
	how_found_site string,
	message string) bodyStruct {

	templateOptions := templateOptionsStruct{Name: name,
		Email:          email,
		Phone:          phone,
		Subject:        subject,
		How_found_site: how_found_site,
		Message:        message}
	emailOptions := emailOptionsStruct{To: os.Getenv("ADMIN_EMAIL")}
	return bodyStruct{TemplateName: "contact",
		TemplateOptions: templateOptions,
		EmailOptions:    emailOptions}
}

func sendContact(name string, email string, phone string, subject string, how_found_site string, message string) Contact {
	methodLog := logging.Log.WithFields(log.Fields{
		"method":         "sendContact",
		"name":           name,
		"email":          email,
		"phone":          phone,
		"subject":        subject,
		"how_found_site": how_found_site,
		"message":        message,
	})

	bodyStruct := prepareBody(name, email, phone, subject, how_found_site, message)
	b, _ := json.Marshal(bodyStruct)
	fmt.Println(string(b))
	bodyReader := strings.NewReader(string(b))
	resp, err := http.Post("http://mail:4444/email/send", "application/json", bodyReader)
	if err != nil {
		errorResponse := "Error while sending e-mail"
		methodLog.Error(errorResponse)
		return Contact{error: &errorResponse}
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errorResponse := "Error while reading mail server data"
		methodLog.Error(errorResponse)
		return Contact{error: &errorResponse}
	}
	dat := mailServerResponse{}
	if err = json.Unmarshal(body, &dat); err != nil {
		errorResponse := "Error while decoding mail server response"
		methodLog.Error(errorResponse)
		return Contact{error: &errorResponse}
	}
	if dat.Error != nil {
		errorResponse := "Error raised by the mail server: " + *dat.Error
		methodLog.Error(errorResponse)
		return Contact{error: &errorResponse}

	}

	return Contact{error: nil}
}

func (c Contact) Error() *string {
	return c.error
}
