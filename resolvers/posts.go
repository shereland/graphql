package resolvers

type Post struct {
	slug         string
	title        string
	category     string
	categorySlug string
	content      string
	publishedAt  string
	url          string
	author       User
	comments     []Comment
	relatedBooks []Book
	relatedPosts []*Post
	album        Album
}

func (p Post) Slug() string {
	return p.slug
}

func (p Post) Title() string {
	return p.title
}

func (p Post) Category() string {
	return p.category
}

func (p Post) CategorySlug() string {
	return p.categorySlug
}

func (p Post) Comments() *[]Comment {
	comments := p.comments
	return &comments
}

func (p Post) Content() string {
	return p.content
}

func (p Post) PublishedAt() string {
	return p.publishedAt
}

func (p Post) Url() string {
	return p.url
}

func (p Post) RelatedBooks() *[]Book {
	return &p.relatedBooks
}

func (p Post) RelatedPosts() *[]*Post {
	return &p.relatedPosts
}

func (p Post) Album() *Album {
	return &p.album
}

func (p Post) Author() *User {
	return &p.author
}

type Posts struct {
	currentPage int32
	lastPage    int32
	data        *[]*Post
}

func (p Posts) CurrentPage() int32 {
	return p.currentPage
}

func (p Posts) LastPage() int32 {
	return p.lastPage
}

func (p Posts) Data() *[]*Post {
	return p.data
}

type Comment struct {
	name      string
	site      string
	message   string
	createdAt string
}

func (c Comment) Name() *string {
	return &c.name
}
func (c Comment) Site() *string {
	return &c.site
}
func (c Comment) Message() *string {
	return &c.message
}
func (c Comment) CreatedAt() *string {
	return &c.createdAt
}
