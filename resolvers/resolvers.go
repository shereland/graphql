package resolvers

import (
	"context"
	"time"

	"gitlab.com/shereland/graphql/generic"
	"gitlab.com/shereland/graphql/metrics"
)

type Resolver struct{}

// LatestTweets return the last two tweets from _shereland
func (r *Resolver) LatestTweets() *[]*Tweet {
	startingTime := time.Now()

	tweetsArray := make([]*Tweet, 0, 2)
	tweets := generic.GetLatestTweets()
	for _, tweet := range tweets {
		t := Tweet{tweet}
		tweetsArray = append(tweetsArray, &t)
	}

	metrics.ResolverDurations.WithLabelValues("LatestTweets").Observe(time.Since(startingTime).Seconds() * 1000)
	return &tweetsArray
}

// Notifications get all notifications for specific user
func (r *Resolver) Notifications() *NotificationResolver {
	m := NotificationResolver{}
	return &m
}

// Post return a single post by slug
func (r *Resolver) Post(args struct{ Slug string }) *Post {
	startingTime := time.Now()
	p := generic.GetPost(args.Slug)
	if p == nil {
		return nil
	}
	comments := make([]Comment, 0, 2)
	if p["comments"] != nil {
		for _, c := range p["comments"].([]map[string]string) {
			comment := Comment{
				name:      c["name"],
				site:      c["site"],
				message:   c["content"],
				createdAt: c["createdAt"]}
			comments = append(comments, comment)
		}
	}
	var relatedBooks []Book
	if p["relatedBooks"] != nil {
		relatedBooks = make([]Book, 0, 2)
		for _, r := range p["relatedBooks"].([]map[string]string) {
			relatedBook := Book{
				title: r["title"],
				link:  r["link"]}
			relatedBooks = append(relatedBooks, relatedBook)
		}
	}
	var relatedPosts []*Post
	if p["relatedPosts"] != nil {
		relatedPosts = make([]*Post, 0, 2)
		for _, r := range p["relatedPosts"].([]map[string]string) {
			relatedPost := Post{
				title: r["title"],
				url:   r["link"]}
			relatedPosts = append(relatedPosts, &relatedPost)
		}
	}
	var album Album
	if p["album"] != nil {
		album = Album{}
		album.title = p["album"].(map[string]interface{})["title"].(string)
		album.photos = make([]*Photo, 0, 10)
		for _, photo := range p["album"].(map[string]interface{})["photos"].([]map[string]string) {
			newphoto := Photo{}
			newphoto.title = photo["title"]
			newphoto.description = photo["alt"]
			newphoto.image = photo["image"]
			newphoto.thumbnail = photo["thumbnail"]
			album.photos = append(album.photos, &newphoto)
		}
	}

	author := p["author"].(map[string]interface{})
	post := Post{
		slug:         p["slug"].(string),
		title:        p["title"].(string),
		content:      p["content"].(string),
		category:     p["category"].(string),
		categorySlug: p["categorySlug"].(string),
		publishedAt:  p["publishedAt"].(string),
		url:          p["url"].(string),
		author:       User{username: author["username"].(string)},
		comments:     comments,
		relatedBooks: relatedBooks,
		relatedPosts: relatedPosts,
		album:        album}

	metrics.ResolverDurations.WithLabelValues("Post").Observe(time.Since(startingTime).Seconds() * 1000)
	return &post
}

// Posts return all posts
// Filter by `category` and/or paginate by `page`
func (r *Resolver) Posts(args struct {
	Category *string
	Page     *int32
}) *Posts {
	startingTime := time.Now()
	posts := make([]*Post, 0, 4)

	var page int32
	if args.Page != nil {
		page = *args.Page
	} else {
		page = 1
	}
	var categorySlug string
	if args.Category != nil {
		categorySlug = *args.Category
	} else {
		categorySlug = ""
	}

	numberOfPagesMessage := make(chan int32)
	go func() { numberOfPagesMessage <- generic.GetNumberOfPages(categorySlug) }()

	for _, p := range generic.GetPosts(categorySlug, page) {
		comments := make([]Comment, 0, 2)
		if p["comments"] != nil {
			for _, c := range p["comments"].([]map[string]string) {
				comment := Comment{
					name:      c["name"],
					site:      c["site"],
					message:   c["content"],
					createdAt: c["createdAt"]}
				comments = append(comments, comment)
			}
		}
		var relatedBooks []Book
		if p["relatedBooks"] != nil {
			relatedBooks = make([]Book, 0, 2)
			for _, r := range p["relatedBooks"].([]map[string]string) {
				relatedBook := Book{
					title: r["title"],
					link:  r["link"]}
				relatedBooks = append(relatedBooks, relatedBook)
			}
		}
		var relatedPosts []*Post
		if p["relatedPosts"] != nil {
			relatedPosts = make([]*Post, 0, 2)
			for _, r := range p["relatedPosts"].([]map[string]string) {
				relatedPost := Post{
					title: r["title"],
					url:   r["link"]}
				relatedPosts = append(relatedPosts, &relatedPost)
			}
		}
		var album Album
		if p["album"] != nil {
			album = Album{}
			album.title = p["album"].(map[string]interface{})["title"].(string)
			album.photos = make([]*Photo, 0, 10)
			for _, photo := range p["album"].(map[string]interface{})["photos"].([]map[string]string) {
				newphoto := Photo{}
				newphoto.title = photo["title"]
				newphoto.description = photo["alt"]
				newphoto.image = photo["image"]
				newphoto.thumbnail = photo["thumbnail"]
				album.photos = append(album.photos, &newphoto)
			}
		}

		author := p["author"].(map[string]interface{})
		post := Post{
			slug:         p["slug"].(string),
			title:        p["title"].(string),
			content:      p["content"].(string),
			publishedAt:  p["publishedAt"].(string),
			url:          p["url"].(string),
			author:       User{username: author["username"].(string)},
			comments:     comments,
			relatedBooks: relatedBooks,
			relatedPosts: relatedPosts,
			album:        album}
		posts = append(posts, &post)
	}

	numberOfPages := <-numberOfPagesMessage

	metrics.ResolverDurations.WithLabelValues("Posts").Observe(time.Since(startingTime).Seconds() * 1000)
	return &Posts{currentPage: page, lastPage: numberOfPages, data: &posts}
}

// PopularPosts Return array of posts (max 5) ordered by the most popular first
func (r *Resolver) PopularPosts() *[]*Post {
	startingTime := time.Now()
	postsArray := make([]*Post, 0, 5)
	posts := generic.GetPopularPosts()
	for _, post := range posts {
		p := Post{title: post["title"], url: post["url"]}
		postsArray = append(postsArray, &p)
	}
	metrics.ResolverDurations.WithLabelValues("PopularPosts").Observe(time.Since(startingTime).Seconds() * 1000)
	return &postsArray
}

type Tweet struct {
	content string
}

func (t Tweet) Content() string {
	return t.content
}

type NotificationResolver struct{}

// FriendshipRequests get all friendship requests to user
func (n NotificationResolver) FriendshipRequests(ctx context.Context) *[]*friendshipRequestsResolver {
	startingTime := time.Now()
	sessionid := ctx.Value("sessionid").(string)
	notifications := generic.FriendshipNotifications(sessionid)
	notificationsArray := make([]*friendshipRequestsResolver, 0, 2)
	for _, notification := range notifications {
		f := friendshipRequestsResolver{slug: notification["slug"], name: notification["name"]}
		notificationsArray = append(notificationsArray, &f)
	}
	metrics.ResolverDurations.WithLabelValues("FriendshipRequests").Observe(time.Since(startingTime).Seconds() * 1000)
	return &notificationsArray
}

type friendshipRequestsResolver struct {
	slug string
	name string
}

func (f friendshipRequestsResolver) Slug() string {
	return f.slug
}

func (f friendshipRequestsResolver) Name() string {
	return f.name
}

type ContactInput struct {
	Name           string
	Email          string
	Phone          *string
	Subject        string
	How_found_site string
	Message        string
}

func (r *Resolver) Contact(args struct{ ContactInput ContactInput }) *Contact {
	startingTime := time.Now()

	contact := sendContact(args.ContactInput.Name, args.ContactInput.Email, *args.ContactInput.Phone, args.ContactInput.Subject, args.ContactInput.How_found_site, args.ContactInput.Message)

	metrics.ResolverDurations.WithLabelValues("Contact").Observe(time.Since(startingTime).Seconds() * 1000)
	return &contact
}
