package resolvers

type User struct {
	username string
}

func (u *User) Username() string {
	return u.username
}
